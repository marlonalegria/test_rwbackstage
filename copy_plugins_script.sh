#!/bin/bash
# Define repository URLs
FIRST_REPO_URL="https://gitlab.com/marlonalegria/test_rwbackstage.git"
SECOND_REPO_URL="https://gitlab.com/marlonalegria/test_plugins.git"
# Define temporary directory for cloning
TEMP_DIR=$(mktemp -d)
CLONE_DIR="$TEMP_DIR/plugins"
echo "PUNTO 1"
# Clone the second repository
git clone "$SECOND_REPO_URL" "$CLONE_DIR"
echo "PUNTO 2"
echo "ESTO TIENE:"
echo "-----------"
echo "$CLONE_DIR"
echo "-----------"
# Copy new plugins to the first repository
cp -r "$CLONE_DIR"/* /plugins
echo "PUNTO 3"
# Navigate to the first repository
cd /plugins
echo "PUNTO 4"
# Add, commit, and push changes
git add .
git commit -m "Added new plugins"
git push origin main
echo "PUNTO 5"
# Clean up temporary directory
rm -rf "$TEMP_DIR"
echo "PUNTO 6"